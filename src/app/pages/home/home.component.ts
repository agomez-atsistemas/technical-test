import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PageEvent } from '@angular/material/paginator';
import { Subject, Observable } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';
import { HeroesService } from '@shared/services/heroes/heroes.service';
import { HeroInterface } from '@shared/interfaces/hero.interface';
import { ConfigPaginator } from '@shared/interfaces/config-paginator';
import { DialogConfirmComponent } from '@shared/components/dialog-confirm/dialog-confirm.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit, OnDestroy {
  public heroes$: Observable<HeroInterface[]>;
  public lengthPaginator: number;
  public configPaginator: ConfigPaginator;
  public dataNotFound: boolean = false;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private heroesService: HeroesService,
    private router: Router,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.configPaginator = {
      previousPageIndex: 0,
      pageIndex: 0,
      pageSize: 5,
      length: this.lengthPaginator
    }
    this.heroes$ = this.heroesService.getHeroes()
    .pipe(
      takeUntil(this.unsubscribe$),
      tap((heroes: HeroInterface[]) => {
        this.lengthPaginator = heroes.length;
        this.changePage(this.configPaginator);
      })
    );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public seeHero(id: number): void {
    this.router.navigate(['/hero', id]);
  }

  public createHero(): void {
    this.router.navigate(['/configure-hero']);
  }

  public updateHero(id: number): void {
    this.router.navigate(['/configure-hero', id]);
  }

  public deleteHero(id: number): void {
    const dialogRef = this.dialog.open(DialogConfirmComponent, {
      data: {
        textMessage: '¿Está seguro de que desea eliminar este superhéroe?',
        iconName: 'warning',
        accept: 'Confirmar',
        decline: 'Cancelar',
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'Confirmar') {
        this.heroesService.deleteHero(id);
        this.snackBar.open('Superhéroe eliminado con éxito', 'X', {
          duration: 4000,
          verticalPosition: 'bottom',
          horizontalPosition: 'center'
        });
      }
    });
  }

  public searchUpdateHeroes(heroesSearch: HeroInterface[]): void {
    this.dataNotFound = heroesSearch.length === 0 ? true : false;
    this.changePage(this.configPaginator, heroesSearch);
  }

  private changePage(event: PageEvent, searchHero = []): void {
    if (searchHero.length > 0) {
      this.heroes$ = this.heroes$.pipe(map((heroes: HeroInterface[]) => heroes = searchHero));
    } else {
      this.heroes$ = this.heroesService.getHeroes();
    }
    const initialIndex = event.pageIndex === 0 ? 0 : event.pageIndex * event.pageSize;
    let finalIndex = event.pageIndex === 0 ? event.pageSize : (event.pageIndex * event.pageSize) + event.pageSize;
    let lengthHeroesList: number = this.heroesService.getHeroes().getValue().length;

    if (finalIndex > lengthHeroesList) {
      finalIndex = lengthHeroesList;
    }
    this.heroes$ = this.heroes$.pipe(map((heroes: HeroInterface[]) => heroes.slice(initialIndex, finalIndex)));
  }

}
