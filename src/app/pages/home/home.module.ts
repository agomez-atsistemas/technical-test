import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { HomeRoutingModule } from '@app/pages/home/home-routing.module';
import { HomeComponent } from '@app/pages/home/home.component';
import { SharedModule } from '@shared/shared.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
    imports: [
        HomeRoutingModule,
        SharedModule,
        MatCardModule,
        MatPaginatorModule,
        MatDialogModule,
        MatSnackBarModule,
    ],
    declarations: [HomeComponent]
})
export class HomeModule {}