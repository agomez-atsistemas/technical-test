import { NgModule } from '@angular/core';
import { ConfigureHeroRoutingModule } from '@app/pages/configure-hero/configure-hero-routing.module';
import { ConfigureHeroComponent } from '@app/pages/configure-hero/configure-hero.component';
import { SharedModule } from '@shared/shared.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        ConfigureHeroRoutingModule,
        SharedModule,
        ReactiveFormsModule,
        MatSelectModule,
    ],
    declarations: [ConfigureHeroComponent]
})
export class ConfigureHeroModule {}