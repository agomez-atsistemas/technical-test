import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { HeroesService } from '@shared/services/heroes/heroes.service';
import { HeroInterface } from '@shared/interfaces/hero.interface';

@Component({
  selector: 'app-configure-hero',
  templateUrl: './configure-hero.component.html',
  styleUrls: ['./configure-hero.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfigureHeroComponent implements OnInit, OnDestroy {
  public configuration: boolean;
  public heroId: number;
  public hero$: Observable<HeroInterface>;
  private unsubscribe$ = new Subject<void>();
  public heroForm: FormGroup;
  name = new FormControl('', [Validators.required]);
  bio = new FormControl('', [Validators.required, Validators.maxLength(200)]);
  img = new FormControl('', [Validators.required,]);
  appearance = new FormControl('', [Validators.required, Validators.pattern(/\d{1,2}\/\d{1,2}\/\d{4}/gm)]);
  distributor = new FormControl('Marvel', [Validators.required]);

  constructor(
    private activatedRoute: ActivatedRoute,
    private heroesService: HeroesService,
    private router: Router,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    this.heroId = +this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.configuration = this.heroId === 0 ? false : true;
    this.heroForm = this.fb.group({
      name: this.name,
      bio: this.bio,
      img: this.img,
      appearance: this.appearance,
      distributor: this.distributor
    });
    if (this.configuration) {
      this.hero$ = this.heroesService.getHeroById(this.heroId)
      .pipe(
        takeUntil(this.unsubscribe$),
        tap((hero: HeroInterface) => this.initForm(hero))
      )
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public initForm(hero: HeroInterface): void {
    if (this.configuration) {
      this.name.patchValue(hero.name);
      this.bio.patchValue(hero.bio);
      this.img.patchValue(hero.img);
      this.appearance.patchValue(hero.appearance);
      this.distributor.patchValue(hero.distributor);
    }
  }
  
  public configureHero(id: number): void {
    if (this.configuration) {
      this.heroesService.updateHero(this.heroForm.value, id);
      this.snackBar.open('Superhéroe editado con éxito', 'X', {
        duration: 4000,
        verticalPosition: 'bottom',
        horizontalPosition: 'center'
      });
      this.router.navigate(['/home']);
    } else {
      this.heroesService.createHero(this.heroForm.value);
      this.snackBar.open('Superhéroe creado con éxito', 'X', {
        duration: 4000,
        verticalPosition: 'bottom',
        horizontalPosition: 'center'
      });
      this.router.navigate(['/home']);
    }
  }

  public backToHome(): void {
    this.router.navigate(['/home']);
  }

}
