import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigureHeroComponent } from '@app/pages/configure-hero/configure-hero.component';

const routes: Routes = [
    {
        path: ':id',
        component: ConfigureHeroComponent
    },
    {
        path: '',
        component: ConfigureHeroComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConfigureHeroRoutingModule {}