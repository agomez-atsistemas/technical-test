import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { HeroesService } from '@shared/services/heroes/heroes.service';
import { HeroInterface } from '@shared/interfaces/hero.interface';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeroComponent implements OnInit, OnDestroy {
  public hero$: Observable<HeroInterface>;
  public heroId: number;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private heroesService: HeroesService,
    private router: Router
  ) {
    this.heroId = +this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.hero$ = this.heroesService.getHeroById(this.heroId)
    .pipe(
      takeUntil(this.unsubscribe$)
    );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public backToHome(): void {
    this.router.navigate(['/home']);
  }

}
