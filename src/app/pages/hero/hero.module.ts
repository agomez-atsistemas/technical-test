import { NgModule } from '@angular/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { HeroRoutingModule } from '@app/pages/hero/hero-routing.module';
import { HeroComponent } from '@app/pages/hero/hero.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
    imports: [
        HeroRoutingModule,
        SharedModule,
        MatGridListModule,
    ],
    declarations: [HeroComponent]
})
export class HeroModule {}