import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('@app/pages/home/home.module').then(module => module.HomeModule)
  },
  { 
    path: 'hero',
    loadChildren: () => import('@app/pages/hero/hero.module').then(module => module.HeroModule)
  },
  {
    path: 'configure-hero',
    loadChildren: () => import('@app/pages/configure-hero/configure-hero.module').then(module => module.ConfigureHeroModule)
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'home'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
