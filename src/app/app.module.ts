import { NgModule } from '@angular/core';
import { AppRoutingModule } from '@app/app-routing.module';
import { HomeModule } from '@app/pages/home/home.module';
import { HeroModule } from '@app/pages/hero/hero.module';
import { ConfigureHeroModule } from '@app/pages/configure-hero/configure-hero.module';
import { AppComponent } from '@app/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    HomeModule,
    HeroModule,
    ConfigureHeroModule
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
