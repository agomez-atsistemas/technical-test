import { Directive, ElementRef, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[formControl][appUppercase]',
})
export class UppercaseDirective {
  constructor(public ref: ElementRef, private control: NgControl) {}

  @HostListener('input', ['$event.target'])
  public onInput(input: HTMLInputElement) {
    if (
      this.control.control?.value &&
      typeof this.control.control.value === 'string'
    ) {
      const caretPos = input.selectionStart;
      this.control.control.setValue(this.control.control.value.toUpperCase(), {
        emitEvent: false,
      });
      input.setSelectionRange(caretPos, caretPos);
    }
  }
}
