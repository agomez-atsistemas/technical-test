export interface HeroInterface {
    readonly id: number;
    readonly name: string;
    readonly bio: string;
    readonly img: string;
    readonly appearance: string;
    readonly distributor: string;
}