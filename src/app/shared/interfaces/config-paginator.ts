export interface ConfigPaginator {
    readonly previousPageIndex: number;
    readonly pageIndex: number;
    readonly pageSize: number;
    readonly length: number;
}
