import { NgModule } from '@angular/core';
import { DialogConfirmComponent } from '@shared/components/dialog-confirm/dialog-confirm.component';
import { DialogLoaderComponent } from '@shared/components/dialog-loader/dialog-loader.component';
import { NavbarComponent } from '@shared/components/navbar/navbar.component';
import { TruncatePipe } from '@shared/pipes/truncate.pipe';
import { UppercaseDirective } from '@shared/directives/uppercase.directive';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
    imports: [
        RouterModule,
        MatToolbarModule,
        MatProgressSpinnerModule,
        CommonModule,
        MatButtonModule,
        MatIconModule,
        MatDividerModule,
        MatInputModule,
        MatFormFieldModule,
    ],
    declarations: [
        DialogConfirmComponent,
        DialogLoaderComponent,
        NavbarComponent,
        TruncatePipe,
        UppercaseDirective        
    ],
    exports: [
        DialogConfirmComponent,
        DialogLoaderComponent,
        NavbarComponent,
        TruncatePipe,
        UppercaseDirective,
        CommonModule,
        MatButtonModule,
        MatIconModule,
        MatDividerModule,
        MatInputModule,
        MatFormFieldModule
    ]
})
export class SharedModule { }