import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HeroesService } from '@shared/services/heroes/heroes.service';
import { HeroInterface } from '@shared/interfaces/hero.interface';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Output() heroesResult = new EventEmitter<HeroInterface[]>();
  public homePage: boolean;
  public filtered: boolean = false;
  @ViewChild('textSearch') textSearchHero;

  constructor(
    private heroesService: HeroesService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.homePage = this.router.url.includes('/home') ? true : false;
  }

  public searchHero(search: string = ''): void {
    this.heroesResult.emit(this.heroesService.searchHeroes(search));
    if (search !== '') {
      this.filtered = true;
    } else {
      this.filtered = false;
      this.textSearchHero.nativeElement.value = '';
    }
  }

}
