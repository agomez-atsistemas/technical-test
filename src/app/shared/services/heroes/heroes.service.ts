import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { HeroInterface } from '@shared/interfaces/hero.interface';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  public heroesData: HeroInterface[] = [
    {
      id: 1,
      name: "Aquaman",
      bio: "El poder más reconocido de Aquaman es la capacidad telepática para comunicarse con la vida marina, la cual puede convocar a grandes distancias.",
      img: "assets/img/aquaman.png",
      appearance: "01/11/1941",
      distributor: "DC"
    },
    {
      id: 2,
      name: "Batman",
      bio: "Los rasgos principales de Batman se resumen en «destreza física, habilidades deductivas y obsesión». La mayor parte de las características básicas de los cómics han variado por las diferentes interpretaciones que le han dado al personaje.",
      img: "assets/img/batman.png",
      appearance: "01/05/1939",
      distributor: "DC"
    },
    {
      id: 3,
      name: "Daredevil",
      bio: "Al haber perdido la vista, los cuatro sentidos restantes de Daredevil fueron aumentados por la radiación a niveles superhumanos, en el accidente que tuvo cuando era niño. A pesar de su ceguera, puede \"ver\" a través de un \"sexto sentido\" que le sirve como un radar similar al de los murciélagos.",
      img: "assets/img/daredevil.png",
      appearance: "01/10/1964",
      distributor: "Marvel"
    },
    {
      id: 4,
      name: "Hulk",
      bio: "Su principal poder es su capacidad de aumentar su fuerza hasta niveles prácticamente ilimitados a la vez que aumenta su furia. Dependiendo de qué personalidad de Hulk esté al mando en ese momento (el Hulk Banner es el más débil, pero lo compensa con su inteligencia).",
      img: "assets/img/hulk.png",
      appearance: "01/05/1962",
      distributor:"Marvel"
    },
    {
      id: 5,
      name: "Linterna Verde",
      bio: "Poseedor del anillo de poder que posee la capacidad de crear manifestaciones de luz sólida mediante la utilización del pensamiento. Es alimentado por la Llama Verde (revisada por escritores posteriores como un poder místico llamado Starheart), una llama mágica contenida en dentro de un orbe (el orbe era en realidad un meteorito verde de metal que cayó a la Tierra, el cual encontró un fabricante de lámparas llamado Chang)",
      img: "assets/img/linterna-verde.png",
      appearance: "01/06/1940",
      distributor: "DC"
    },
    {
      id: 6,
      name: "Spider-Man",
      bio: "Tras ser mordido por una araña radiactiva, obtuvo los siguientes poderes sobrehumanos, una gran fuerza, agilidad, poder trepar por paredes. La fuerza de Spider-Man le permite levantar 10 toneladas o más. Gracias a esta gran fuerza Spider-Man puede realizar saltos íncreibles. Un \"sentido arácnido\", que le permite saber si un peligro se cierne sobre él, antes de que suceda. En ocasiones este puede llevar a Spider-Man al origen del peligro.",
      img: "assets/img/spiderman.png",
      appearance: "01/08/1962",
      distributor: "Marvel"
    },
    {
      id: 7,
      name: "Wolverine",
      bio: "En el universo ficticio de Marvel, Wolverine posee poderes regenerativos que pueden curar cualquier herida, por mortal que ésta sea, además ese mismo poder hace que sea inmune a cualquier enfermedad existente en la Tierra y algunas extraterrestres . Posee también una fuerza sobrehumana, que si bien no se compara con la de otros superhéroes como Hulk, sí sobrepasa la de cualquier humano.",
      img: "assets/img/wolverine.png",
      appearance: "01/11/1974",
      distributor: "Marvel"
    }
  ];
  private heroes$ = new BehaviorSubject<HeroInterface[]>(this.heroesData);

  constructor() {
    const heroesInLocalStorage: HeroInterface[] = this.getDataOfLocalStorage();
    if (heroesInLocalStorage === null || heroesInLocalStorage.length === 0) {
      this.saveDataInLocalStorage();
    } else {
      this.heroes$.next(heroesInLocalStorage);
    }
  }

  public getDataOfLocalStorage(): HeroInterface[] {
    return JSON.parse(localStorage.getItem('heroes'));
  }

  public saveDataInLocalStorage(heroes: HeroInterface[] = []): void {
    heroes.length > 0 ? localStorage.setItem('heroes', JSON.stringify(heroes)) : localStorage.setItem('heroes', JSON.stringify(this.heroesData));
  }

  public searchHeroes(search: string = ''): HeroInterface[] {
    let searchedHeroes: HeroInterface[] = [];

    if(search === '') {
      return this.heroes$.getValue();
    } else {
      search = search.toLowerCase();
      const heroesCopy: HeroInterface[] = [...this.heroes$.getValue()];
      heroesCopy.forEach((hero: HeroInterface) => {
        let name = hero.name.toLocaleLowerCase();
  
        if (name.includes(search)) {
          searchedHeroes.push(hero);
        }
      });
      return searchedHeroes;
    }
  }

  public getHeroes(): BehaviorSubject<HeroInterface[]> {
    return this.heroes$;
  }

  public getHeroById(id: number): Observable<HeroInterface> {
    return this.heroes$.pipe(
      map((heroes: HeroInterface[]) => {
        return heroes.find((hero: HeroInterface) => hero.id === id);
      })
    )
  }

  public createHero(hero: HeroInterface): void {
    const newHero: HeroInterface = {
      id: this.generateNewId(),
      name: hero.name,
      bio: hero.bio,
      img: hero.img,
      appearance: hero.appearance,
      distributor: hero.distributor
    };
    let heroesCopy: HeroInterface[] = [...this.heroes$.getValue()];
    heroesCopy.push(newHero);
    this.saveDataInLocalStorage(heroesCopy);
    this.heroes$.next(heroesCopy);
  }

  public updateHero(hero: HeroInterface, id: number): void {
    const updatedHero: HeroInterface = { id, ...hero };
    const indexUpdatedHero = this.getHeroByIndex(id);
    let heroesCopy: HeroInterface[] = [...this.heroes$.getValue()];
    heroesCopy.splice(indexUpdatedHero, 1, updatedHero);
    this.saveDataInLocalStorage(heroesCopy);
    this.heroes$.next(heroesCopy);
  }

  public deleteHero(id: number): void {
    const indexHeroToDelete = this.getHeroByIndex(id);
    let heroesCopy: HeroInterface[] = [...this.heroes$.getValue()];
    heroesCopy.splice(indexHeroToDelete, 1);
    this.saveDataInLocalStorage(heroesCopy);
    this.heroes$.next(heroesCopy);
  }

  private getHeroByIndex(id: number): number {
    const heroesInLocalStorage: HeroInterface[] = this.getDataOfLocalStorage();
    return heroesInLocalStorage.findIndex((hero: HeroInterface) => hero.id === id);
  }

  private generateNewId(): number {
    let newId: number;
    let existingsIds: number[] = [];
    const heroesInLocalStorage: HeroInterface[] = this.getDataOfLocalStorage();
    heroesInLocalStorage.forEach((hero: HeroInterface) => {
      return existingsIds.push(hero.id);
    });
    newId = Math.max(...existingsIds) + 1;
    return newId;
  }
}
